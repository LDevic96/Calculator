package hr.ferit.ldevic.prvaaplikacija;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText etPrviBroj;
    EditText etDrugiBroj;
    Button bZbroji;
    Button bOduzmi;
    Button bPomnozi;
    Button bPodjeli;
    TextView tvRezultat;

    private void initializeUI()
    {
        this.etPrviBroj= (EditText) findViewById(R.id.etPrviBroj);
        this.etDrugiBroj= (EditText) findViewById(R.id.etDrugiBroj);
        this.bZbroji= (Button) findViewById(R.id.bZbroji);
        this.bOduzmi= (Button) findViewById(R.id.bOduzmi);
        this.bPomnozi= (Button) findViewById(R.id.bPomnozi);
        this.bPodjeli= (Button) findViewById(R.id.bPodjeli);
        this.tvRezultat = (TextView) findViewById(R.id.tvRezultat);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
    }

    private void displayRezultat(double rezultat)
    {
        this.tvRezultat.setText(String.valueOf(rezultat));
    }

    public void zbroji(View view)
    {
        String broj1string = this.etPrviBroj.getText().toString();
        double broj1double = Double.parseDouble(broj1string);
        String broj2string = this.etDrugiBroj.getText().toString();
        double broj2double = Double.parseDouble(broj2string);
        double rezultat = broj1double + broj2double;
        this.displayRezultat(rezultat);
    }
    public void oduzmi(View view)
    {
        String broj1string = this.etPrviBroj.getText().toString();
        double broj1double = Double.parseDouble(broj1string);
        String broj2string = this.etDrugiBroj.getText().toString();
        double broj2double = Double.parseDouble(broj2string);
        double rezultat = broj1double - broj2double;
        this.displayRezultat(rezultat);
    }
    public void pomnozi(View view)
    {
        String broj1string = this.etPrviBroj.getText().toString();
        double broj1double = Double.parseDouble(broj1string);
        String broj2string = this.etDrugiBroj.getText().toString();
        double broj2double = Double.parseDouble(broj2string);
        double rezultat = broj1double * broj2double;
        this.displayRezultat(rezultat);
    }
    public void podjeli(View view)
    {
        String broj1string = this.etPrviBroj.getText().toString();
        double broj1double = Double.parseDouble(broj1string);
        String broj2string = this.etDrugiBroj.getText().toString();
        double broj2double = Double.parseDouble(broj2string);
        double rezultat = broj1double / broj2double;
        this.displayRezultat(rezultat);
    }
}
